﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RafaMarket.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RafaMarket
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            // Isto é para inicializar a BD e fazer as migrações primeiro
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.RafaMarketContext, Migrations.Configuration>());
            // Usar a data context criado pelo asp.net para o processo de autenticação
            ApplicationDbContext db = new ApplicationDbContext();
            CreateRoles(db);
            // Função para criar SuperUser
            CreateSuperUser(db);
            // Adicinar permissões ao SuperUser
            AddPermissionsToSuperUser(db);
            // Fechar ligação
            db.Dispose();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void AddPermissionsToSuperUser(ApplicationDbContext db)
        {
            // Ligação aos Users
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            // Ligação aos Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            // Procurar o user SuperAdmin
            var user = userManager.FindByName("paulo.afg@outlook.pt");

            // Verifica se foi atribuido a role View ao user
            if (!userManager.IsInRole(user.Id, "View"))
            {
                // Atribui a role View ao user
                userManager.AddToRole(user.Id, "View");
            }

            // Verifica se foi atribuido a role Create ao user
            if (!userManager.IsInRole(user.Id, "Create"))
            {
                // Atribui a role Create ao user
                userManager.AddToRole(user.Id, "Create");
            }

            // Verifica se foi atribuido a role Edit ao user
            if (!userManager.IsInRole(user.Id, "Edit"))
            {
                // Atribui a role Edit ao user
                userManager.AddToRole(user.Id, "Edit");
            }

            // Verifica se foi atribuido a role Delete ao user
            if (!userManager.IsInRole(user.Id, "Delete"))
            {
                // Atribui a role Delete ao user
                userManager.AddToRole(user.Id, "Delete");
            }
        }

        private void CreateSuperUser(ApplicationDbContext db)
        {
            // Ligação aos Users
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            // Procurar o user
            var user = userManager.FindByName("paulo.afg@outlook.pt");

            // Ver se Superuser já existe
            if (user == null)
            {
                // Criar user
                user = new ApplicationUser
                {
                    UserName = "paulo.afg@outlook.pt",
                    Email = "paulo.afg@outlook.pt",
                };

                // Criar o user com a senha Senhac30!
                userManager.Create(user,"Senhac30!");
            }
        }

        private void CreateRoles(ApplicationDbContext db)
        {
            // RoleManager - objeto que permite criar o roles
            // Os roles vão estar criados num objeto do tipo RoleStore
            // Ligação aos Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            // Para apagar uma role pelo códifo
            // var role = roleManager.FindByName("Create");
            // roleManager.Delete(role);

            // Verifica se a role existe e se não existe cria
            if (!roleManager.RoleExists("View"))
            {
                roleManager.Create(new IdentityRole("View"));
            }

            if (!roleManager.RoleExists("Edit"))
            {
                roleManager.Create(new IdentityRole("Edit"));
            }

            if (!roleManager.RoleExists("Create"))
            {
                roleManager.Create(new IdentityRole("Create"));
            }

            if (!roleManager.RoleExists("Delete"))
            {
                roleManager.Create(new IdentityRole("Delete"));
            }
        }
    }
}

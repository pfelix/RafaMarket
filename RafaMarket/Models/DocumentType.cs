﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class DocumentType
    {
        [Key]
        [Display(Name ="Tipo Documento")] // É o nome que vai aparecer na view
        public int DocumentTypeID { get; set; }

        [Display(Name = "Documento")]
        public string Description { get; set; }

        // Propriedade que vai ligar à classe Employee
        // Neste caso fazemos o ICollection porque um documento vai ter vários empregados
        // Mas um empregado só tem um documento, por isso na classe Employye temos uma propriedade do tipo virtual DocumentType
        public virtual ICollection<Employee> Employees { get; set; }

        // Ligação à tabela Customer
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
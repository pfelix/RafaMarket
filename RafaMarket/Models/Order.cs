﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }

        public DateTime DataOrder { get; set; }

        public int CustomerID { get; set; }

        public OrderStatus OrderStatus { get; set; }

        // Liação à tabela Customer
        public virtual Customer Customer { get; set; }

        //Ligação à tabela OrderDetail
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
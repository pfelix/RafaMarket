﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class RafaMarketContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public RafaMarketContext() : base("name=RafaMarketContext")
        {
        }

        // Por defeito é aplicada a regra cascade no delete, ou seja se apagar um registo relacionado apaga tudo
        // Criamos este metodo para remover esta regra para isto não acontecer
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<RafaMarket.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.DocumentType> DocumentTypes { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.Employee> Employees { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.Supplier> Suppliers { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.Order> Orders { get; set; }

        public System.Data.Entity.DbSet<RafaMarket.Models.OrderDetail> OrdersDetails { get; set; }
    }
}

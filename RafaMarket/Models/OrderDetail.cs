﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class OrderDetail
    {
        [Key]
        public int OrderDetailID { get; set; }

        public int OrderID { get; set; }

        public int ProductId { get; set; }

        [Display(Name ="Descrição do Produto")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres.", MinimumLength = 3)]
        [Required(ErrorMessage ="Tem que inserir um {0}")]
        public string Description { get; set; }

        [Display(Name ="Preço")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public decimal Price { get; set; }

        [Display(Name = "Quantidade")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        public float Quantity { get; set; }

        // Ligação à tabela Order
        public virtual Order Order { get; set; }

        // Ligação à tabela Product
        public virtual Product Product { get; set; }
    }
}
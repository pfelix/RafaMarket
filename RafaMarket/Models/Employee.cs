﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    //[Table("Empregados")] // Para mudar o nome da tabela na BD
    public class Employee
    {
        //[Column("IdEmpregado")] // Para mudar o nome do campo na BD
        [Key]
        public int EmployeeID { get; set; }

        [StringLength(30, ErrorMessage = "O {0} deverá ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Display(Name ="Primeiro Nome")]
        [Required(ErrorMessage ="Tem que inseriro o {0}")]
        public string FirstName { get; set; }

        [StringLength(30, ErrorMessage ="O {0} deverá ter entre {2} e {1} carateres", MinimumLength =3)]
        [Display(Name = "Apelido")]
        [Required(ErrorMessage = "Tem que inseriro o {0}")]
        public string LastName { get; set; }

        [Display(Name = "Salário")]
        [Required(ErrorMessage ="Tem que inserir um valor para o {0}")] // Obrigar o preenchimento dp campo
        public decimal Salary { get; set; }

        [Display(Name = "Percentagem Bónus")]
        [Range(0,20, ErrorMessage ="O valor da {0} deverá ser entre {1} e {2}")]
        public float BonusPercent { get; set; }

        [Display(Name = "Data Nascimento")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Data Inicio")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        [DataType(DataType.Date)]
        public DateTime StartTime { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string URL { get; set; }

        // Propriedade que vai guardar o dado do DocumentType
        [Display(Name = "Tipo de Documento")]
        [Range(1, double.MaxValue, ErrorMessage = "Tem que selecionar um {0}")] // Para resolver o problema do não ser selecionado o documento.
        [Required(ErrorMessage = "O {0} é necessário")]
        //[ForeignKey("")] // Em vez da propriedade virtual para fazer a ligação 
        public int DocumentTypeID { get; set; }
       
        [NotMapped] // Este metodo é usado como modelo de dados, mas não é incluido na base de dados
        public int Age
        {
            get
            {

                var myAge = DateTime.Now.Year - DateOfBirth.Year;

                // Verifica se já passou o dia de anos do ano corrente
                if (DateOfBirth > DateTime.Now.AddYears(-myAge)) // Vai buscar a data atual e soma aos anos os anos que já passaram, ou seja, retira por ser -myAge
                {
                    myAge--;
                }

                return myAge;
            }
        }

        // Propriedade que vai fazer a ligação à classe DocumentType
        // Por este motivo ser virtual
        public virtual DocumentType DocumentType { get; set; }

    }
}
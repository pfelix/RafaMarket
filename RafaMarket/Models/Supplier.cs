﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class Supplier
    {
        [Key]
        public int SupplierID { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage ="Tem que introduzir um {0} para o Fornecedor")]
        [StringLength(30,ErrorMessage ="O campo {0} deverá conter entre {2} e {1} carateres", MinimumLength = 3)]
        public string Name { get; set; }

        [Display(Name ="Nome do Contacto")]
        [Required(ErrorMessage = "Tem que introduzir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres", MinimumLength = 3)]
        public string ContactFristName { get; set; }

        [Display(Name = "Apelido do Contacto")]
        [Required(ErrorMessage = "Tem que introduzir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres", MinimumLength = 3)]
        public string ContactLastName { get; set; }

        [Display(Name = "Telefone")]
        [Required(ErrorMessage = "Tem que introduzir um {0}")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres", MinimumLength = 9)]
        public string Phone { get; set; }

        [Display(Name = "Morada")]
        [Required(ErrorMessage = "Tem que introduzir um {0}")]
        [StringLength(100, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres", MinimumLength = 3)]
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        // Ligação á tabela SupplierProduct
        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Display(Name = "Descrição")]
        [StringLength(30,ErrorMessage ="A {0} deverá ter entre {2} e {1}", MinimumLength =3)] // Para controlar a quantidade de carateres
        [Required(ErrorMessage ="Deve inserir uma {0}")]
        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString ="{0:C2}",ApplyFormatInEditMode =false)]// ApplyFormatInEditMode = false - para guardar na tabela no formato da tabela
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [Display(Name ="Preço")]
        public decimal Price { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}",ApplyFormatInEditMode =true)]
        [Display(Name = "Última Compra")]
        public DateTime LastBuy { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public float Stock { get; set; }

        // Para as imagens
        public string Image { get; set; }

        // Propriedade que serve para carregar a imagem
        [NotMapped]
        public HttpPostedFileBase ProductImageUrl { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        // Ligação á tabela SupplierProduct
        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }

        // Ligação à tabela OrderDetail
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }

        [Display(Name ="Primeiro Nome")]
        [StringLength(30, ErrorMessage ="O campo {0} deverá conter entre {2} e {1} carateres.", MinimumLength =3)]
        [Required(ErrorMessage ="Tem que inserir um {0}")]
        public string FirstName { get; set; }

        [Display(Name = "Apelido")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} carateres.", MinimumLength = 3)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public string LastName { get; set; }

        [Display(Name ="Nome")]
        [NotMapped] // Para não guardar na tabela mas utilizar no programa
        public string Name { get { return $"{FirstName} {LastName}"; } }

        [Display(Name = "Telefone")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} digitos.", MinimumLength = 9)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Morada")]
        [StringLength(100, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} digitos.", MinimumLength = 3)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Nº Documento")]
        [StringLength(20, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} digitos.", MinimumLength = 5)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public string Document { get; set; }

        [Display(Name ="Tipo de Documento")]
        [Range(1, double.MaxValue, ErrorMessage = "Tem que selecionar um {0}")] // Para resolver o problema do não ser selecionado o documento.
        [Required(ErrorMessage ="O {0} é necessário")]
        public int DocumentTypeID { get; set; }

        // Liação à tabela DocumentTypeID
        public virtual DocumentType DocumentType { get; set; }

        // Liação à tabela Order
        public virtual ICollection<Order> Orders { get; set; }
    }
}
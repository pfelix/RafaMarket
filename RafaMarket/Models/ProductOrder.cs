﻿using System.ComponentModel.DataAnnotations;

namespace RafaMarket.Models
{
    public class ProductOrder : Product
    {
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]// ApplyFormatInEditMode = false - para guardar na tabela no formato da tabela
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [Display(Name = "Quantidade")]
        public float Quantity { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]// ApplyFormatInEditMode = false - para guardar na tabela no formato da tabela
        [Display(Name = "Valor")]
        // Como não tem o set não guarda na Base de dados, caso contrário temos que utilizar o [NotMapped]
        public decimal Value {
            get{
                return Price * (decimal)Quantity;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaMarket.Models
{
    public class SupplierProduct
    {
        [Key]
        public int SupplierProductID { get; set; }

        public int SupplierID { get; set; }

        public int ProductId { get; set; }

        // Ligação á tabela Supplier
        public virtual Supplier Supplier { get; set; }

        // Ligação á tabela Product
        public virtual Product Product { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RafaMarket.Startup))]
namespace RafaMarket
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

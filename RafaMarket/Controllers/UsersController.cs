﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RafaMarket.Helpers;
using RafaMarket.Models;
using RafaMarket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RafaMarket.Controllers
{
    public class UsersController : Controller
    {
        // Ligação á Base de dados através do contexte Default
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Users
        // Mostrar lista de utilizadores
        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            // Lista de Users existentes
            var users = userManager.Users.ToList();

            // Lista do tipo UserView
            var usersView = new List<UserView>();

            // Passar para a View
            foreach (var user in users)
            {
                var userView = new UserView
                {
                    Email = user.Email,
                    Name = user.UserName,
                    UserId = user.Id
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        // GET: Roles/5
        // Mostrar as roles atribuidas ao utilizador
        public ActionResult Roles (string userID)
        {
            // Verifica se userID sem vazio
            if(string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Ligação aos Users
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            // ligção aos Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var users = userManager.Users.ToList();

            // Procurar o user
            var user = users.Find(u => u.Id == userID);

            // Verifica se userID existe
            if(user == null)
            {
                return HttpNotFound();
            }

            // Lista para a RoleView
            var rolesView = new List<RoleView>();

            // carregar a RoleView com as permissõs dos user encontrado
            foreach (var item in user.Roles)
            {
                // Procurar as roles do user
                var role = roles.Find(r => r.Id == item.RoleId);

                var roleView = new RoleView
                {
                    RoleId = role.Id,
                    Name = role.Name
                };

                rolesView.Add(roleView);
            }

            // Criar a View com os dados
            var userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                UserId = user.Id,
                Roles = rolesView
            };

            return View(userView);

        }

        // GET: AddRole?userID=5
        public ActionResult AddRole (string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                UserId = user.Id
            };

            ViewBag.RoleId = new SelectList(CombosHelper.GetRoles() , "Id", "Name");

            return View(userView);
        }

        // POST: AddRole?userID=5
        [HttpPost]
        public ActionResult AddRole(string userID, FormCollection form)
        {
            var roleID = Request["RoleId"];

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            var userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                UserId = user.Id
            };

            // Quando o Model é um viewModel temos que fazer as validações na action, porque não se está a utilizar o identety
            if (string.IsNullOrEmpty(roleID))
            {
                ViewBag.Error = "Tem que selecionar uma permissão";
                ViewBag.RoleId = new SelectList(CombosHelper.GetRoles(), "Id", "Name");
                return View(userView);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roles = roleManager.Roles.ToList();
            var role = roles.Find(r => r.Id == roleID);

            // Verificar se user já tem a permissão e adicionar na tabela
            if (!userManager.IsInRole(userID, role.Name))
            {
                userManager.AddToRole(userID, role.Name);
            }

            // Montar a RoleView
            var rolesView = new List<RoleView>();
                        
            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    Name = role.Name,
                    RoleId = role.Id
                };

                rolesView.Add(roleView);
            }

            // Montar a UserView
            userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                Roles = rolesView,
                UserId = user.Id
            };
            

            return View("Roles", userView);
        }

        public ActionResult Delete (string userID, string roleID)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(roleID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var role = roleManager.Roles.ToList().Find(r => r.Id == roleID);

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = userManager.Users.ToList().Find(u => u.Id == userID);

            // Verificar se utilizador tem a role
            if (userManager.IsInRole(user.Id, role.Name))
            {
                // apagar a role do user
                userManager.RemoveFromRole(user.Id, role.Name);
            }

            // Preparar a View
            var users = userManager.Users.ToList();
            var roles = roleManager.Roles.ToList();

            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    Name = role.Name,
                    RoleId = role.Id
                };

                rolesView.Add(roleView);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                Roles = rolesView,
                UserId = user.Id
            };

            return View("Roles", userView);

        }

        /// <summary>
        /// Metodo que é utilizado para fechar a ligação depois de utilizar algum dos metodos no contrulado
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
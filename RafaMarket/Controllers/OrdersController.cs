﻿using RafaMarket.Helpers;
using RafaMarket.Models;
using RafaMarket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RafaMarket.Controllers
{
    public class OrdersController : Controller
    {
        private RafaMarketContext db = new RafaMarketContext();

        // GET: NewOrder
        public ActionResult NewOrder()
        {
            var orderView = new OrderView(); // Cria um objeto do tipo OrderView
            orderView.Customer = new Customer();
            orderView.Products = new List<ProductOrder>();

            // Variável de sessão - para conseguir passar os dados entre as view/controller
            Session["orderView"] = orderView;

            // Carregar a lista de Customers
            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");
            // "CustomerID" - Dado que é guardado?
            // "Name" - Dado que é mostrado?

            return View(orderView);
        }

        // POST: NewOrder
        [HttpPost]
        public ActionResult NewOrder(OrderView orderView)
        {
            orderView = Session["orderView"] as OrderView;

            // Verificar se cliente foi selecionado
            var customerID = int.Parse(Request["CustomerID"]);

            if (customerID == 0)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");
                ViewBag.Error = "Deve selecionar um cliente";

                return View(orderView);
            }

            // Verificar se o cliente existe
            var customer = db.Customers.Find(customerID);

            if (customer == null)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");
                ViewBag.Error = "O cliente não existe";

                return View(orderView);
            }

            // Verificar se existe detalhe de encomenda
            if (orderView.Products.Count == 0)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");
                ViewBag.Error = "Deve escolher os produtos a encomendar";

                return View(orderView);
            }

            int orderID = 0;

            // BeginTransaction - É para o sistema poder voltar atrás caso acorra algum erro,
            // para isso no final do processo temos que fazer [variável].Commit() para validar ou [variável].Rollback() para voltar atrás
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    // Gravar a encomenda
                    var order = new Order
                    {
                        CustomerID = customerID,
                        DataOrder = DateTime.Now,
                        OrderStatus = OrderStatus.Created
                    };

                    db.Orders.Add(order);

                    db.SaveChanges();

                    // Gravar o detalhe das encomendas

                    orderID = order.OrderID;

                    foreach (var item in orderView.Products)
                    {
                        var orderDetail = new OrderDetail
                        {
                            Description = item.Description,
                            Price = item.Price,
                            Quantity = item.Quantity,
                            OrderID = orderID,
                            ProductId = item.ProductID
                        };

                        db.OrdersDetails.Add(orderDetail);
                        db.SaveChanges();
                    }

                    transaction.Commit(); // Valida a transaction
                }
                catch (Exception e)
                {
                    transaction.Rollback(); // Volta atrás com a transaction

                    ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");
                    ViewBag.Error = $"Erro: {e.Message}";
                    return View(orderView);

                }
            }

            ViewBag.Message = $"A encomenda: {orderID} foi efetuada com sucesso!";

            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");

            // return RedirectToAction("NewOrder"); - desta forma não vai mostrar a Mensagem
            // Tem que ser da seguinte forma
            orderView = new OrderView();
            orderView.Customer = new Customer();
            orderView.Products = new List<ProductOrder>();

            Session["orderView"] = orderView;

            return View(orderView);
        }

        // GET: AddProduct
        public ActionResult AddProduct()
        {
            // Carregar a lista de Products
            ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");

            return View();
        }
        
        // POST: AddProduct
        [HttpPost]
        public ActionResult AddProduct(ProductOrder productOrder)
        {
            // Vamos buscar o varíável de sessão, mas como vem como objeto temos que fazer o cast com base no model, neste caso será com base no ViewModel > OrderView
            var orderView = Session["orderView"] as OrderView;

            // Guardar o Id do produto escolhido na comboBox.
            // O Request trás o valor do ProductID, mas vem como objeto e o ID é int, temos então de fazer um parse
            // Podemos ir buscar logo ao prodctOrder, foi para aprender o Request...
            var productID = int.Parse(Request["ProductID"]);

            // Se não for selecionado nenhum produto
            if (productID == 0)
            {
                ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");
                ViewBag.Error = "Deve selecionar um produto";

                return View(productOrder);
            }

            // Verifica se produto existe
            var product = db.Products.Find(productID);

            if (product == null)
            {
                ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");
                ViewBag.Error = "O produto selecionado não existe";

                return View(productOrder);
            }

            // Verificar se o produto já existe na lista
            productOrder = orderView.Products.Find(p => p.ProductID == productID);

            if (productOrder == null)
            {
                // Como na view não temos todos os campos de todas as propriédades ProductOrder e Product, temos que completar
                productOrder = new ProductOrder
                {
                    Description = product.Description,
                    Price = product.Price,
                    ProductID = product.ProductID,
                    Quantity = float.Parse(Request["Quantity"])
                };

                // Passar os dados para a vire NewOrder
                // Adiciona o produto à variável de sessão
                orderView.Products.Add(productOrder);
            }
            else
            {
                productOrder.Quantity += float.Parse(Request["Quantity"]);
            }

            // É necessário voltar a popular a comoBox dos clientes
            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersNames(), "CustomerID", "Name");

            return View("NewOrder", orderView);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
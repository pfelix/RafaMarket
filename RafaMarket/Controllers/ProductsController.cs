﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RafaMarket.Helpers;
using RafaMarket.Models;

namespace RafaMarket.Controllers
{
    // [Authorize] // Quer dizer que é necessário autorização para utilizar o controlador, tanto se pode meter no controlador como nas action
    // [AllowAnonymous]
    public class ProductsController : Controller
    {
        private RafaMarketContext db = new RafaMarketContext();

        // GET: Products
        [Authorize(Roles ="View")]
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,Description,Price,LastBuy,Stock,Image,ProductImageUrl,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();

                if (product.ProductImageUrl != null)
                {
                    var folder = "~/Content/Images";
                    var file = string.Format("{0}.png", product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageUrl, folder, file);

                    // Se UploadImage com sucesso
                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);
                        product.Image = pic;
                        
                        // Atualizar a BD, ou seja, estamos a fazer um EDIT
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,Description,Price,LastBuy,Stock,Image,ProductImageUrl,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.ProductImageUrl != null)
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Images";
                    var file = string.Format("{0}.png", product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageUrl, folder, file);

                    // Se UploadImage com sucesso
                    if (response)
                    {
                        pic = string.Format("{0}/{1}", folder, file);
                        product.Image = pic;
                    }
                }

                // Atualizar dados na BD
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Metodo que é utilizado para fechar a ligação depois de utilizar algum dos metodos no contrulado
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

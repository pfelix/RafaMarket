﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RafaMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RafaMarket.Helpers
{
    public class CombosHelper:IDisposable // Herda de uma iterface IDisposable para fecharmos a ligação
    {
        private static RafaMarketContext db = new RafaMarketContext();

        private static ApplicationDbContext da = new ApplicationDbContext();

        /// <summary>
        /// Metoto que retorna a lista dos tipos de documentos
        /// /// E na possição 0 [Selecione um tipo de documento...]
        /// </summary>
        /// <returns></returns>
        public static List<DocumentType> GetDocumentTypes() // Como este metodo é static o context também tem de ser static
        {
            var DocumentTypes = db.DocumentTypes.ToList();

            DocumentTypes.Add(new DocumentType
            {
                DocumentTypeID = 0,
                Description = "[Selecione um tipo de documento...]"
            });

            return DocumentTypes.OrderBy(d => d.Description).ToList();
        }

        /// <summary>
        /// Metodo que retorna a lista de todos os cliente
        /// E na possição 0 [Selecione um cliente...]
        /// </summary>
        /// <returns></returns>
        public static List<Customer> GetCustomersNames()
        {
            // Ir buscar todos os clientes
            var Customers = db.Customers.ToList();
            Customers.Add(new Customer
            {
                CustomerID = 0,
                FirstName = "[Selecione um cliente...]"
            });

            return Customers.OrderBy(c => c.Name).ToList();
        }

        /// <summary>
        /// Metodo que retorna a lista de todos os produtos
        /// E na possição 0 [Selecione um produto...]
        /// </summary>
        /// <returns></returns>
        public static List<Product> GetProducts()
        {
            var Products = db.Products.ToList();
            Products.Add(new Product
            {
                ProductID = 0,
                Description = "[Selecione um produto...]"

            });

            return Products.OrderBy(p => p.Description).ToList();
        }


        /// <summary>
        /// Metodo que retorna a lista de todas as roles
        /// E na possição 0 [Selecione um produto...]
        /// </summary>
        /// <returns></returns>
        public static List<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(da));
            var list = roleManager.Roles.ToList();
            list.Add(new IdentityRole
            {
                Id = "",
                Name = "[Selecione uma permissão]"
            });

            return list.OrderBy(r => r.Name).ToList();
        }

        /// <summary>
        /// Metodo herdado da interface para fechar a ligação do data context
        /// </summary>
        public void Dispose()
        {
            db.Dispose();
        }
    }
}
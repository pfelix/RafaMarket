namespace RafaMarket.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RafaMarket.Models.RafaMarketContext>
    {
        public Configuration()
        {
            // Para fazer a migra��o autom�ticamente, sem termos que ir � consola
            // Na primeira fez na consola metemos Enable-Migrations -ContextTypeName [Nome do data context] -EnableAutomaticMigrations
            AutomaticMigrationsEnabled = true;

            // Para permitir que sejam perdidos dados
            // Exp. Mudar do tipo flout para int, vai perder dados
            // Se n�o metermos isto o programa p�ra
            AutomaticMigrationDataLossAllowed = true;

            // Para indicar o caminho do data context [namespace].[Nome do data context]
            ContextKey = "RafaMarket.Models.RafaMarketContext";
        }

        protected override void Seed(RafaMarket.Models.RafaMarketContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
